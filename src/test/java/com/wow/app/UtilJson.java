package com.wow.app;

import com.google.gson.Gson;
import com.wow.app.models.Game;
import com.wow.app.models.UserStat;
import org.apache.http.HttpResponse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class UtilJson {
    private final static Gson GSON = new Gson();

    public static Game getGame(HttpResponse response) throws IOException {
        return GSON.fromJson(getJson(response), Game.class);
    }

    public static UserStat getGamesStat(HttpResponse response) throws IOException {
        return GSON.fromJson(getJson(response), UserStat.class);
    }

    private static String getJson(HttpResponse response) throws IOException {
        StringBuilder sb = new StringBuilder();
        new BufferedReader(new InputStreamReader((response.getEntity().getContent()))).lines().forEach(sb::append);
        return sb.toString();
    }
}
