package com.wow.app;

import com.google.gson.Gson;
import com.wow.app.models.Game;
import com.wow.app.models.User;
import com.wow.app.models.UserMove;
import com.wow.app.models.UserStat;
import com.wow.app.models.rules.Rules;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.HttpStatus;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLOutput;
import java.util.*;

public class GameTest {
    private final static String PORT = "8091";
    private final static String BASE_URL = "http://localhost:" + PORT;
    private final static List<User> USERS = Arrays.asList(new User("Seva"), new User("Alex"), new User("Dime"));
    private final static Gson GSON = new Gson();
    private final static HttpClient httpclient = HttpClients.createDefault();
    private final static Random RANDOM = new Random();

    @Test
    public void basicTest() throws IOException {
        for(int i = 0; i < 100000; i++) {
            Game createdGame = createGame();
            makeMoves(createdGame);
            Game game = terminateGame(createdGame);
            System.out.println(game);
        }

        for(User user : USERS) {
            getStat(user);
        }
    }

    private Game createGame() throws IOException {
        HttpPost httpPost = new HttpPost(BASE_URL + "/game/create");
        httpPost.addHeader("Content-Type", "application/json");
        String users = GSON.toJson(USERS);

        StringEntity stringEntity = new StringEntity(users);
        httpPost.setEntity(stringEntity);
        HttpResponse response = httpclient.execute(httpPost);

        Assert.assertTrue(response.getStatusLine().getStatusCode() == HttpStatus.CREATED.value());
        return UtilJson.getGame(response);
    }

    private void makeMoves(Game game) throws IOException {
        for (UserMove userMove : game.getUserMoves()) {
            StringBuilder sb = new StringBuilder();
            sb.append("id=").append(game.getId()).append("&");
            sb.append("userName=").append(userMove.getUser().getName()).append("&");
            String move = Rules.POSSIBLE_MOVES.get(RANDOM.nextInt(3));
            sb.append("move=").append(move);
            HttpPut httpPut = new HttpPut(BASE_URL + "/game/move?" + sb.toString());
            httpPut.addHeader("Content-Type", "application/json");
            HttpResponse response = httpclient.execute(httpPut);
            System.out.println(UtilJson.getGame(response).toString());
        }
    }

    private Game terminateGame(Game game) throws IOException {
        HttpPut httpPut2 = new HttpPut(BASE_URL + "/game/terminate/" + game.getId());
        httpPut2.addHeader("Content-Type", "application/json");
        HttpResponse response = httpclient.execute(httpPut2);
        Assert.assertTrue(response.getStatusLine().getStatusCode() == HttpStatus.OK.value());
        return UtilJson.getGame(response);
    }

    private void getStat(User user) throws IOException {
        HttpGet httpGet = new HttpGet(BASE_URL + "/game/userStat/" + user.getName());
        httpGet.addHeader("Content-Type", "application/json");
        HttpResponse response = httpclient.execute(httpGet);
        UserStat userStat = UtilJson.getGamesStat(response);
        System.out.println("---------------------------");
        System.out.println(userStat);
        System.out.println("---------------------------");
    }
}
