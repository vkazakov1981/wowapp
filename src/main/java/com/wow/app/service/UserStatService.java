package com.wow.app.service;

import com.wow.app.models.Game;
import com.wow.app.models.User;
import com.wow.app.models.UserMove;
import com.wow.app.models.UserStat;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserStatService {

    private final Map<User, UserStat> userStats = new HashMap<>();

    public void putStatistic(Game game) {
        for (UserMove move : game.getUserMoves()) {
            String userName = move.getUser().getName();
            UserStat userStat = createOrGetExist(userName);
            userStat.incrementTotalGames();
            Integer gameResult = move.getResult();
            switch (gameResult) {
                case -1:
                    userStat.incrementLose();
                    break;
                case 0:
                    userStat.incrementDraw();
                    break;
                case 1:
                    userStat.incrementWon();
                    break;
            }
        }
    }

    public UserStat createOrGetExist(String userName) {
        User newUser = new User(userName);
        return userStats.computeIfAbsent(newUser, e -> new UserStat(newUser));
    }

    public Collection<UserStat> getAllStat() {
        return userStats.values();
    }
}
