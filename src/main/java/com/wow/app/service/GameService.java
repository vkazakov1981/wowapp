package com.wow.app.service;

import com.wow.app.models.Game;
import com.wow.app.models.User;
import com.wow.app.models.UserMove;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
public class GameService {
    private final AtomicInteger id = new AtomicInteger(1000);
    private final Map<Integer, Game> games = new HashMap<>();

    public Game createGame(Collection<User> users) {
        List<UserMove> userMoves = users.stream().map(user -> new UserMove(user, "none")).collect(Collectors.toList());
        Game newGame = new Game(id.incrementAndGet(), userMoves);

        games.put(newGame.getId(), newGame);
        return newGame;
    }

    public Game get(int id) {
        return games.get(id);
    }

    public Collection<Game> getAll() {
        return games.values();
    }
}
