package com.wow.app.models;

public class UserMove {
    private User user;
    private String move;
    private Integer result;

    public UserMove(User user, String move) {
        this.user = user;
        this.move = move;
        this.result = null;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setMove(String move) {
        this.move = move;
    }

    public User getUser() {
        return user;
    }

    public String getMove() {
        return move;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

    public Integer getResult() {
        return this.result;
    }

    @Override
    public String toString() {
        return "UserMove{" +
                "user=" + user +
                ", move='" + move + '\'' +
                ", result=" + result +
                '}';
    }
}
