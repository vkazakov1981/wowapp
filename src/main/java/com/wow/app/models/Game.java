package com.wow.app.models;

import com.wow.app.models.rules.Rules;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class Game {
    private final int id;
    private final Collection<UserMove> userMoves;

    private boolean terminated = false;

    public Game(int id, Collection<UserMove> userMoves) {
        this.id = id;
        this.userMoves = userMoves;
    }

    public int getId() {
        return this.id;
    }

    public Collection<UserMove> getUserMoves() {
        return userMoves;
    }

    @Override
    public String toString() {
        return "Game{" +
                "id=" + id +
                ", userMoves=" + userMoves +
                ", terminated=" + terminated +
                '}';
    }

    public void terminate() {
        List<UserMove> lost = new ArrayList<>();
        for (UserMove userMove1 : getUserMoves()) {
            for (UserMove userMove2 : getUserMoves()) {
                if (userMove1 != userMove2) {
                    int result = Rules.getPairResult(userMove1.getMove(), userMove2.getMove());
                    if (result == -1) {
                        userMove1.setResult(-1);
                        lost.add(userMove1);
                    }
                }
            }
        }
        if (lost.size() == 0) {
            for (UserMove userMove : getUserMoves()) {
                userMove.setResult(0);
            }
        }
        if (lost.size() >= 1) {
            for (UserMove userMove : getUserMoves()) {
                if (!lost.contains(userMove)) {
                    userMove.setResult(1);
                }
            }
        }
        terminated = true;
    }
}
