package com.wow.app.models.rules;

import java.util.*;

public class Rules {
    public final static List<String> POSSIBLE_MOVES = new ArrayList<>(Arrays.asList("stone", "paper", "scissors"));

    public final static Map<String, String> LOST_PAIRS = new HashMap<>();
    static {
        LOST_PAIRS.put("scissors", "stone");
        LOST_PAIRS.put("stone", "paper");
        LOST_PAIRS.put("paper", "scissors");
    }

    /**
     * -1 lost
     * 0 equals
     * 1 won
     */
    public static int getPairResult(String move1, String move2) {
        if (move1.equals(move2)) {
            return 0;
        }
        String value = LOST_PAIRS.get(move1);
        return value.equals(move2) ? -1 : 1;
    }
}
