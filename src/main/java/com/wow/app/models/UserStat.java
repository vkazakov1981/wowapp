package com.wow.app.models;

public class UserStat {
    private User user;

    private int totalGamesPlayed = 0;
    private int won = 0;
    private int draw = 0;
    private int lose = 0;

    public UserStat() {

    }

    public UserStat(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public int getTotalGamesPlayed() {
        return totalGamesPlayed;
    }

    public int getWon() {
        return won;
    }

    public int getDraw() {
        return draw;
    }

    public int getLose() {
        return lose;
    }


    public void setUser(User user) {
        this.user = user;
    }

    public void setTotalGamesPlayed(int totalGamesPlayed) {
        this.totalGamesPlayed = totalGamesPlayed;
    }

    public void setWon(int won) {
        this.won = won;
    }

    public void setDraw(int draw) {
        this.draw = draw;
    }

    public void setLose(int lose) {
        this.lose = lose;
    }

    public void incrementTotalGames() {
        this.totalGamesPlayed++;
    }

    public void incrementLose() {
        this.lose++;
    }

    public void incrementDraw() {
        this.draw++;
    }

    public void incrementWon() {
        this.won++;
    }

    @Override
    public String toString() {
        return "UserStat{" +
                "user=" + user +
                ", totalGamesPlayed=" + totalGamesPlayed +
                ", won=" + won +
                ", draw=" + draw +
                ", lose=" + lose +
                '}';
    }
}
