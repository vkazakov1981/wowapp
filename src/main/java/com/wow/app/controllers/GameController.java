package com.wow.app.controllers;

import com.wow.app.models.Game;
import com.wow.app.models.User;
import com.wow.app.models.UserMove;
import com.wow.app.models.UserStat;
import com.wow.app.service.GameService;
import com.wow.app.service.UserStatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/game")
public class GameController {
    @Autowired
    private GameService gameService;

    @Autowired
    private UserStatService userStatService;


    @PostMapping("/create")
    public ResponseEntity<Game> create(@RequestBody List<User> users) {
        Game newGame = gameService.createGame(users);
        return new ResponseEntity<>(newGame, HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Game> get(@PathVariable int id) {
        Game existingGame = gameService.get(id);
        return new ResponseEntity<>(existingGame, existingGame != null ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @PutMapping("/move")
    public ResponseEntity<Game> move(@RequestParam int id, @RequestParam String userName, @RequestParam String move) {
        Game game = gameService.get(id);
        if (game != null) {
            Optional<UserMove> userMove = game.getUserMoves().stream().filter(item -> item.getUser().getName().equals(userName)).findFirst();
            if (userMove.isPresent()) {
                userMove.get().setMove(move);
                return new ResponseEntity<>(game, HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/terminate/{id}")
    public ResponseEntity<Game> terminate(@PathVariable int id) {
        Game game = gameService.get(id);
        if (game != null) {
            game.terminate();
            userStatService.putStatistic(game);
            return new ResponseEntity<>(game, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/userStat/{userName}")
    public ResponseEntity<UserStat> userStat(@PathVariable String userName) {
        UserStat userStat = userStatService.createOrGetExist(userName);
        return new ResponseEntity<>(userStat, HttpStatus.OK);
    }

    @GetMapping("/")
    public ResponseEntity<Collection<Game>> get() {
        return new ResponseEntity<>(gameService.getAll(), HttpStatus.OK);
    }
}
